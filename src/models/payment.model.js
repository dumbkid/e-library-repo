// payment-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const modelName = "payment";
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const schema = new Schema(
    {
      userId: { type: String, required: true },
      accountNumber: { type: String, required: true },
      bankName: { type: String, required: true },
      paymentDate: { type: Date, required: true },
      DepositorName: { type: String, required: true },
      refNumber: { type: String, required: true },
      receiverNumber: { type: String, required: true },
      subscriptionId: { type: String, required: true },
      receiverName: { type: String, required: true },
      company: { type: String, required: true },
    },
    {
      timestamps: true,
    }
  );

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
};
