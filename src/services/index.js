const users = require('./users/users.service.js');
const books = require('./books/books.service.js');
const subscriptions = require('./subscriptions/subscriptions.service.js');
const payment = require('./payment/payment.service.js');
const education = require('./education/education.service.js');
const experience = require('./experience/experience.service.js');
const request = require('./request/request.service.js');
const category = require('./category/category.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(books);
  app.configure(subscriptions);
  app.configure(payment);
  app.configure(education);
  app.configure(experience);
  app.configure(request);
  app.configure(category);
};
